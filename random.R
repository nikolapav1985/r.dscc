x.dat <- sample(20,120,replace=TRUE) # length of vector 120, maximum value 20
x.dat
table(x.dat)
mean(x.dat)
plot(table(x.dat),main="Graphics cards sales, 120 days period")

# EXAMPLE OUTPUT (NUMBER OF GRAPHICS CARDS SOLD, 120 DAYS PERIOD)
#
# NUMBER OF UNITS SOLD IN A DAY
#
#  [1] 18  2 18 12 12  2 19  1  3  1  8 14  7 17 11 11 11  4 14 16 17  9 14  7 20
# [26] 16  6 10  6 13  3 14 14  5 15 12  1 12 12  8  6  7  7 18 10 15  6 11  2 16
# [51]  9 15 15 18 15  8  9 15  5 20 16  4  1  5  1 15 14  2  7  6  1  3  6  6 10
# [76] 15  7  8 13  9 20  5 18 16 19 15  7 14 10  9  6 11 16 15  2  9 13 19  7 16
#[101]  3 15  8  5 14 17  3  8 13 10  7  5 15 17 10  7 16 14  4  7
#
# TABLE OF FREQUENCIES
#
#x.dat
# 1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 
# 6  5  5  3  6  8 11  6  6  6  5  5  4  9 12  8  4  5  3  3 
#
# DATA SET MEAN
#
#[1] 10.3

